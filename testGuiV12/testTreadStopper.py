import threading
import time


class MyThread(threading.Thread):
  # overriding constructor
    def __init__(self, i):
    # calling parent class constructor
        threading.Thread.__init__(self)
        self.x = i
        self.stop = False
    
  # define your own run method
    def run(self):
        while self.stop == False:
            print("Value stored is: ", self.x)
            time.sleep(0.5)
    
    def terminateThread(self):
        self.stop = True
        time.sleep(1)
        print("Exiting thread with value: ", self.x)




thread1 = MyThread(1)
thread1.start()
thread2 = MyThread(2)
thread2.start()

time.sleep(10)

thread1.terminateThread()
thread2.terminateThread()