import sqlite3
from sqlite3 import Error

class DatabaseHandler():
    def __init__(self, dbPath) -> None:

        self.connection = self.create_connection(dbPath)
        #self.connection = self.create_connection("sqlite3/ImageSet.db")
        self.crsr = self.connection.cursor()

        pass

    def create_connection(self,path): 
        connection = None
        try:
            connection = sqlite3.connect(path)
            print("Connection to SQLite DB successful")
        except Error as e:
            print(f"The error '{e}' occurred")
        return connection

    def close(self):
        self.connection.close()

    def selectAllFromTable(self,table):
        sql_command = 'SELECT * FROM ' + str(table) + ';'
        self.crsr.execute(sql_command)
        result = self.crsr.fetchall()
        return result

    def selectLink(self,imageId): 
        sql_command = 'SELECT link FROM images WHERE imageId ="' + imageId + '";'
        self.crsr.execute(sql_command)
        result = self.crsr.fetchall()
        return(result)

    def selectDate(self,table , imageId):
        sql_command = 'SELECT dateTime FROM ' + str(table) + ' WHERE imageId = "'+ imageId +'";'
        self.crsr.execute(sql_command)     
        result = self.crsr.fetchall()
        return(result)

    def selectWhereDateWhereGreater(self,table,date):
        sql_command = 'SELECT dateTime FROM '+ str(table) +' WHERE dateTime >"' +str(date)+'" ORDER BY datetime DESC;'
        self.crsr.execute(sql_command)
        result = self.crsr.fetchall()
        return(result)

    # def updateDayViewSQL(self,startTime,endTime):
    #     sql_command = 'DROP VIEW imagesOnDate;'
    #     self.crsr.execute(sql_command)
    #     sql_command = 'CREATE VIEW imagesOnDate AS SELECT imageId, dateTime FROM images WHERE dateTime BETWEEN "' +str(startTime)+ '" AND "' +str(endTime)+ '" ORDER BY dateTime;'
    #     self.crsr.execute(sql_command)

    def selectHourView(self,startTime, endTime):
        #sql_command = 'SELECT * FROM imagesOnDate WHERE dateTime BETWEEN "' +str(startTime)+ '" AND "' +str(endTime)+ '" ORDER BY dateTime;'
        sql_command = 'SELECT imageId, dateTime FROM images WHERE dateTime BETWEEN "' +str(startTime)+ '" AND "' +str(endTime)+ '" ORDER BY dateTime;'
        self.crsr.execute(sql_command)     
        result = self.crsr.fetchall()
        return(result)

    def selectROIs(self, imageId):
        sql_command = 'SELECT roiX, roiY FROM defects WHERE imageId = "' + imageId + '" ;'
        self.crsr.execute(sql_command)
        result = self.crsr.fetchall()
        return result
    
    def countDefects(self):
        defectArr = []
        sql_command = 'SELECT COUNT(*) FROM headlessDefects;'
        self.crsr.execute(sql_command)
        result = self.crsr.fetchall()
        if len(result) == 0:
            defectArr.append(0)
        else:
            defectArr.append(result[0][0])

        sql_command = 'SELECT COUNT(*) FROM unCutDefects;'
        result = self.crsr.fetchall()
        if len(result) == 0:
            defectArr.append(0)
        else:
            defectArr.append(result[0][0])


        sql_command = 'SELECT COUNT(*) FROM partialDefects;'
        result = self.crsr.fetchall()
        if len(result) == 0:
            defectArr.append(0)
        else:
            defectArr.append(result[0][0])


        sql_command = 'SELECT COUNT(*) FROM clusteredDefects;'
        result = self.crsr.fetchall()
        if len(result) == 0:
            defectArr.append(0)
        else:
            defectArr.append(result[0][0])


        return defectArr



    def insertEntry(self, table, fields):

        sql_command = 'INSERT INTO ' + table + ' VALUES ('

        for i in fields:

            if isinstance(i,str) == True:
                if i == fields[0]:
                    sql_command = sql_command + '"' + i + '"'
                else:
                    sql_command = sql_command + ',"' + i + '"'
            else:
                if i == fields[0]:
                    sql_command = sql_command + str(i)
                else:
                    sql_command = sql_command + ',' + str(i)

        sql_command = sql_command + ');'
        try:
            self.crsr.execute(sql_command)
        except:
            print("enrty allready exists or is incorrect")
        self.connection.commit()

    def removeImageOnId(self,id):
        sql_command = 'DELETE FROM images WHERE imageId = "'+id+'";'
        self.crsr.execute(sql_command)   
        self.connection.commit()