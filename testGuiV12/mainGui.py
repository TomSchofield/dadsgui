# import sqlite3
# from sqlite3 import Error
import threading
import GuiV2
import time
import DatabaseHandler as dbH
import cv2
import defectCapture
#import gst
# from datetime import datetime


# class myThread(threading.Thread):
#     def __init__(self, target: None = ..., args = ..., daemon: None = ...):
#         super().__init__(target=target, args=args, daemon=daemon)
#         self.stop_thread = False

# now = datetime.now()
# hello = str(now)
# i = hello[0:19]
#camSet = ' ! video/x-raw(memory:NVMM), width=4056, height=3040, framerate=10/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width=640, height=854, format = BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink drop = True'
#camset = 'device=/dev/video0 ! video/x-raw-yuv,framerate=30/1,width=1280,height=720 ! xvimagesink'
camset = 'device=/dev/video0 ! videoconvert ! videoscale ! video/x-raw,format=RGB ! queue ! videoconvert ! ximagesink name=img_origin'

#cam = cv2.VideoCapture(camSet) ## nano
#cam = cv2.VideoCapture(0,cv2.CAP_DSHOW)  ## laptop
cam = cv2.VideoCapture(0) 


databaseHandler = dbH.DatabaseHandler("sqlite3/ImageSet.db")


#stop_threads = False


#camReaderThread = threading.Thread(target=defectCapture.imageCapture, args=(0,cam), daemon=True) # make into object
camReaderThread = defectCapture.imageCaptureTread(cam)
camReaderThread.start()
#defectCapture.imageCapture(cam)


#x = threading.Thread(target=GuiV2.GuiThreadStarter, args=(0,cam), daemon=True)
#x = threading.Thread(target=GuiV2.GuiThreadStarter, args=(0,cam), daemon=True)
#x.start()
GuiV2.GuiThreadStarter(cam)

#addAll()
#time.sleep(5)
#removeAll()
#stop_threads = True
#camReaderThread.join()
camReaderThread.terminateThread()
camReaderThread.join()
#time.sleep(2)
cam.release()
databaseHandler.close()
print("connection closed...main")
cv2.destroyAllWindows()

# def makestring(idnum):
#     if idnum < 10:
#         idstring = "im0000" + str(idnum)
#     else:
#         idstring = "im000" + str(idnum)
#     return idstring

# def addAll():
#     idCounter = 1
#     while idCounter < 11:
#         time.sleep(5)

#         idstring = makestring(idCounter+10)

#         if idCounter < 9:
#             min = 7 * idCounter
#             if min < 10:
#                 dateAdded = "2021-08-22 01:0" + str(min) + ":00"
#             else:
#                 dateAdded = "2021-08-22 01:" + str(min) + ":00"
#         else:
#             min = (7 * idCounter) - 60
#             if min < 10:
#                 dateAdded = "2021-08-22 02:0" + str(min) + ":00"
#             else:
#                 dateAdded = "2021-08-22 02:" + str(min) + ":00"

#         databaseHandler.insertEntry("images",[idstring,"FabricImages/Basler_daA1280-54uc__21917873__20200727_153433020_1015",dateAdded,'bt0001'])

#         idCounter = idCounter + 1
        

# def removeAll():
#     idCounter = 1
#     while idCounter < 11:
#         strid = makestring(idCounter+10)
#         databaseHandler.removeImageOnId(strid)

#         idCounter = idCounter + 1

        
#addAll()
#time.sleep(5)
#removeAll()
#databaseHandler.close()
