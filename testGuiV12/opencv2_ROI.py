#opencv2_ROI.py 
#extract the region of interest and show it. 

import cv2
import time
#import gstreamer
print (cv2.__version__)
timeMark = time.time()
dtFil = 0
font = cv2.FONT_HERSHEY_DUPLEX


"""camSet = 'nvarguscamerasrc sensor-id=1 ! video/x-raw(memory:NVMM), width=3264, height=2464, framerate=21/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width=960, height=720, format = BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink'"""

camSet = 'nvarguscamerasrc sensor-id=1 ! video/x-raw(memory:NVMM), width=4056, height=3040, framerate=10/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width=960, height=720, format = BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink drop = True'

""" SENSOR_ID = 1
FRAME_RATE = 29

camSet = 'gst-launch-1.0 nvarguscamerasrc sensor-id=$SENSOR_ID ! "video/x-raw(memory:NVMM),width=4032,height=3040,framerate=$FRAMERATE/1" ! nvvidconv ! "video/x-raw(memory:NVMM),width=1920,height=1080,framerate=$FRAMERATE/1, format = BGRx ! nvvidconv ! video/x-raw, format=BGR ! appsink'

#" ! nvoverlaysink'q"""

#cam = cv2.VideoCapture(camSet)
cam = cv2.VideoCapture(0)

while True:
    _, frame = cam.read()

    #get FPS
    dt=time.time()-timeMark
    timeMark=time.time()
    dtFil=0.9*dtFil+0.1*dt
    fps=1/dtFil
    print('fps: ',fps)

    #put fps in a rectangle
    cv2.rectangle(frame, (0,0),(150,40),(0,0,255),-1)
    cv2.putText(frame,'fps: '+str(round(fps,1)),(0,30),font,1,(0,255,255),2)

    #put the roi rectangle on image
    cv2.rectangle(frame, (40,350),(950,450),(0,255,255),1)
    
    #get ROI (region of interest)
    roi = frame[350:450, 0:950]

    cv2.imshow('myCam',frame)
    cv2.moveWindow('myCam', 0,0)

    cv2.imshow('roi', roi)
    cv2.moveWindow('roi',1000,0)

    if cv2.waitKey(1) ==ord('q'):
        break
cam.release()
cv2.destroyAllWindows()