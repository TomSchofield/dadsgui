#from sqlite3.dbapi2 import connect
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
from PyQt5.QtCore import * #QDateTime, QTimer
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import cv2
import DatabaseHandler as dbH

#import sqlite3
#from sqlite3 import Error
import datetime
import time
import sys

class QIdPushButton(QtWidgets.QPushButton):
    def __init__(self, mainWindow, imageId):
        QtWidgets.QPushButton.__init__(self,mainWindow)
        self.imageId = imageId
        self.active = False
        self.flipped = False

    def getId(self):
        return self.imageId



class DateEdit(QtWidgets.QDateEdit):
    def __init__(self, parent=None):
        super().__init__(parent, calendarPopup=True)
        self._today_button = QtWidgets.QPushButton(self.tr("Today"))
        self._today_button.clicked.connect(self._update_today)
        self.calendarWidget().layout().addWidget(self._today_button)

    @QtCore.pyqtSlot()
    def _update_today(self):
        self._today_button.clearFocus()
        today = QtCore.QDate.currentDate()
        self.calendarWidget().setSelectedDate(today)


class Defect_Gui_App(QtWidgets.QMainWindow):

    def setupUi(self, MainWindow, cam):
        MainWindow.setObjectName("MainWindow")
        self.ui = uic.loadUi('mainWindowv4.ui',self)
        self.frame = QFrame()
        #self.cam = cam

        self.timer = QTimer(self, interval = 150)  # crude soloution
        self.timer.timeout.connect(self.flashDefect)  # crude soloution
        self.flashCounter = 0  # crude soloution

        self.autoClicked = False
        self.liveRio = True
        self.activeId = ""
        #self.lastClicked = ""
        ## added
        #self.connection = create_connection("sqlite3/ImageSet.db")
        self.databaseHandler = dbH.DatabaseHandler("sqlite3/ImageSet.db")
  
        # cursor 
        #self.crsr = self.connection.cursor()

        #self.currentDateTime = datetime.datetime(2021,8,22,0,0,0)
        self.currentDateTime = datetime.datetime(2021,9,25,0,0,0)

        self.lastDefectDateTime = self.currentDateTime

        self.buttonList = []
        self.defectList = []

        self.AutoUpdate.setChecked(True)

        #video 
        self.LiveFeedThread = LiveFeedThread(parent=None,cam=cam)
        self.LiveFeedThread.ImageUpdate.connect(self.UpdateLiveFeed)
        self.LiveFeedThread.start()

        ##### added
        self.checkBox.toggled.connect(lambda:self.btnstate(self.checkBox))
        self.nextButton.clicked.connect(self.nextHour)
        self.previousButton.clicked.connect(self.previousHour)
        self.actionView_Date.triggered.connect(self.ViewDate)
        self.OKButton.clicked.connect(self.changeDate)
        self.LiveButton.clicked.connect(self.DisplayCurrentTime)

        self.dateEdit = DateEdit(self)
        #self.DateClusterHL.addWidget(self.dateEdit) if using layouts
        self.dateEdit.move(20,40)
        self.dateEdit._update_today()


        self.defectThread = ThreadClass(parent=None,lastDefectDateTime = self.lastDefectDateTime)
        self.defectThread.start()
        self.defectThread.update_signal.connect(self.updateAllTags)

        self.makeAllTags() 

        self.RioBox = QtWidgets.QLabel(self.LiveFeedFrame)
        #defectBox.setGeometry(QtCore.QRect(self.ImageLabel.x() + roiX - 70, self.ImageLabel.y() + roiY - 5, 40, 40)) # roix and roiy a bit off
        self.RioBox.setGeometry(QtCore.QRect(0,350,854,100))
        self.RioBox.setStyleSheet("\n"
        "border: 3px solid ;\n"
        "background-color: transparent;\n"
        "border-color: rgb(0, 255, 0);\n")
        self.RioBox.setText("")


        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "testGuiV4"))
        self.LCapLable.setText(_translate("MainWindow", "00:00"))
        self.RCapLable.setText(_translate("MainWindow", "01:00"))
        self.checkBox.setText(_translate("MainWindow", "Hide Defect"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuView.setTitle(_translate("MainWindow", "View"))

    def UpdateLiveFeed(self, LiveFeedFrame, RoiFrame):
        self.LiveFeed.setPixmap(QPixmap.fromImage(LiveFeedFrame))
        if self.liveRio == True:
            self.ImageLabel.setPixmap(QPixmap.fromImage(RoiFrame))


    def makeButton(self, IdsAndDates, firstTime):

        buttonWidth = 32
        buttonHeight = 52

        Id = IdsAndDates[0]
        dateTime = datetime.datetime.strptime(IdsAndDates[1], "%Y-%m-%d %H:%M:%S")

        time_delta = dateTime - self.currentDateTime
        secconds_delta = time_delta.total_seconds()
        if secconds_delta < 10:
            x=0
        else:
            remainder = secconds_delta % 10
            fromStart = (secconds_delta-remainder) / 10
            #x= (fromStart * 3) + self.TimeLine.x() - (buttonWidth / 2)   #76  #40 
            x= (fromStart * 3) - (buttonWidth / 2)

        newButton = QIdPushButton(self.timeLineFrame,Id)
        #newButton.setGeometry(QtCore.QRect(x, self.TimeLine.y() - 23, buttonWidth, buttonHeight))
        newButton.setGeometry(QtCore.QRect(x, 2, buttonWidth, buttonHeight))

        newButton.setStyleSheet("background-color: transparent; border: 0px")
        newButton.setText("")
        icon = QtGui.QIcon()

        if len(self.buttonList) % 2 == 0:
                if Id == "im00002":
                    icon.addPixmap(QtGui.QPixmap("RscImages/LolipopBlue.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                else:  # cheating needs to be changed
                    icon.addPixmap(QtGui.QPixmap("RscImages/LolipopRed.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        else:
                icon.addPixmap(QtGui.QPixmap("RscImages/LolipopRedFlip.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                newButton.flipped = True
                newButton.move(x,self.TimeLine.y()-4)
                #newButton.move(x, 19)

        if self.activeId == Id:
            newButton.active = True
            if newButton.flipped == False:
                icon.addPixmap(QtGui.QPixmap("RscImages/Base.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            else:
                icon.addPixmap(QtGui.QPixmap("RscImages/BaseFlipped.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)


        newButton.setIcon(icon)
        newButton.setIconSize(QtCore.QSize(50, 50))
        newButton.setObjectName("bt"+Id)
        self.buttonList.append(newButton)


        #newButton.clicked.connect( lambda: self.tagClick(newButton.getId()))
        newButton.clicked.connect( lambda: self.tagClick(newButton))

        if firstTime == False:
            newButton.show()


    def makeDefectBox(self,roiX,roiY):
        defctSideLength = 40

        defectBox = QtWidgets.QLabel(self.ImageFrame)
        #defectBox.setGeometry(QtCore.QRect(self.ImageLabel.x() + roiX - 70, self.ImageLabel.y() + roiY - 5, 40, 40)) # roix and roiy a bit off
        defectBox.setGeometry(QtCore.QRect(roiX - (defctSideLength/2),roiY - (defctSideLength/2) , defctSideLength, defctSideLength)) 
        defectBox.setStyleSheet("\n"
        "border: 3px solid ;\n"
        "background-color: transparent;\n"
        "border-color: rgb(255, 0, 0);\n")
        defectBox.setText("")
        #defectBox.setScaledContents(True)
        self.defectList.append(defectBox)

    def makeAllDefectBoxes(self,imageId):  # TEMP SOLUTION

        ROIs = self.databaseHandler.selectROIs(imageId)
        
        for row in ROIs:
                self.makeDefectBox(row[0],row[1])


    def tagClick(self,button):
        self.hideDefectBoxes() ## makes sure old boxes are not kept on screen
        self.defectList.clear()
        icon = QtGui.QIcon()
        imageId = button.imageId
        #button = self.FindButtonOnId(imageId)

        _translate = QtCore.QCoreApplication.translate

        if button.active == True:
            self.DeActivateAllButtons()
        else:
            self.DeActivateAllButtons()

            result = self.databaseHandler.selectDate("images",imageId)
            dateTime = result[0][0]

            self.DefectInfo.setText(_translate("MainWindow", "- Headless Loop" + "\n- " + dateTime)) # must change

            link = self.databaseHandler.selectLink(imageId)
            path = link[0]
            self.ImageLabel.setPixmap(QtGui.QPixmap(path[0]))
            self.makeAllDefectBoxes(imageId)
            self.checkBox.setChecked(False)
            self.btnstate(self.checkBox)
            if button.flipped == False:
                icon.addPixmap(QtGui.QPixmap("RscImages/Base.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            else:
                icon.addPixmap(QtGui.QPixmap("RscImages/BaseFlipped.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            button.setIcon(icon)
            button.active = True
            self.liveRio = False
            self.activeId = button.imageId

    def DeActivateAllButtons(self):
        self.activeId = ""
        self.liveRio = True
        for button in self.buttonList:
            self.DeActivateButton(button)


    def DeActivateButton(self, button):
        icon = QtGui.QIcon()
        button.active = False
        if button.flipped == True:
            icon.addPixmap(QtGui.QPixmap("RscImages/LolipopRedFlip.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        else:
            icon.addPixmap(QtGui.QPixmap("RscImages/LolipopRed.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        button.setIcon(icon)


    def FindButtonOnId(self, imageId):
        for button in self.buttonList:
            if button.getId() == imageId:
                return button

    def btnstate(self,b):
        if b.isChecked() == True:
            self.hideDefectBoxes()
        else:
            self.showDefectBoxes()

    def showButtons(self):
        for button in self.buttonList:
            button.show()

    def showDefectBoxes(self):
        for box in self.defectList:
            box.show()

    def hideDefectBoxes(self):
        for box in self.defectList:
            box.hide()

        
    # def updateDayView(self):
    #     #startTime = self.dateEdit.date()
    #     #self.currentDateTime = startTime
    #     startTime = self.currentDateTime
    #     day = datetime.timedelta(hours=24)
    #     endTime = startTime + day
    #     self.databaseHandler.updateDayViewSQL(startTime,endTime)

    def getHourView(self):
        startTime = self.currentDateTime
        hour = datetime.timedelta(hours=1)
        endTime = startTime + hour
     
        hourView = self.databaseHandler.selectHourView(startTime,endTime)
        return(hourView)

    def updateDefectCounters(self):
        _translate = QtCore.QCoreApplication.translate
        defctCounts = self.databaseHandler.countDefects()
        self.HeadlessCounter.setText(_translate("MainWindow", 'Headless: ' + str(defctCounts[0])))
        self.UncutCounter.setText(_translate("MainWindow", 'Uncut: ' + str(defctCounts[1])))
        self.PartialCounter.setText(_translate("MainWindow", 'Partial: ' + str(defctCounts[2])))
        self.ClusterCounter.setText(_translate("MainWindow", 'Clustered: ' + str(defctCounts[3])))



    def makeAllTags(self):
        IdsDates = self.getHourView()
        for row in IdsDates:
            self.makeButton(row,True)
            self.lastDefectDateTime = datetime.datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")
            self.defectThread.lastDefectDateTime = self.lastDefectDateTime 
            self.updateDefectCounters()


    def clearAllTags(self):
        for button in self.buttonList:
            button.deleteLater()
        self.buttonList.clear()

    
    def updateAllTags(self,int_signal, date_signal):
        if int_signal > 0:
            #buttonId = self.ReturnActiveId()
            if int_signal != 2 and self.AutoUpdate.isChecked() == True:
                temp = datetime.datetime.strptime(date_signal[0][0], "%Y-%m-%d %H:%M:%S")
                self.currentDateTime = self.roundHourDown(temp)
                self.updateCapLables()

            self.clearAllTags()
            IdsDates = self.getHourView()
            if len(IdsDates) != 0:
                for row in IdsDates:
                    self.makeButton(row,False)

                ## can change 
                self.lastDefectDateTime = self.defectThread.lastDefectDateTime

            
                if int_signal == 1:  # should never be more than one defect at at time other that start up
                    self.timer.start() # crude soloution
                    if self.AutoUpdate.isChecked() == True and self.liveRio == True:
                        self.autoClicked = True
                        self.tagClick(self.buttonList[len(self.buttonList)-1])

            self.updateDefectCounters()


    def ReturnActiveId(self):
        for button in self.buttonList:
            if button.active == True:
                return button.imageId
        return False

    def nextHour(self):

        hour = datetime.timedelta(hours=1)
        self.currentDateTime = self.currentDateTime + hour
        self.updateAllTags(2, 0) # signal = 2 when manuel
        self.updateCapLables()


    def updateCapLables(self):
        hour = datetime.timedelta(hours=1)
        _translate = QtCore.QCoreApplication.translate
        self.LCapLable.setText(_translate("MainWindow", str(self.currentDateTime)[11:16]))
        self.RCapLable.setText(_translate("MainWindow", str(self.currentDateTime + hour)[11:16]))


    def previousHour(self):
        hour = datetime.timedelta(hours=1)
        self.currentDateTime = self.currentDateTime - hour
        self.updateAllTags(2, 0) # signal = 2 when manuel
        self.updateCapLables()


    def ViewDate(self):
        print("needs to be implemented")
        # DateSelector = calanderWidget()
        # DateSelector.show()
    
    def changeDate(self):
        temp = self.dateEdit.date().toPyDate()
        temp = str(temp) + " 00:00:00"
        self.currentDateTime = datetime.datetime.strptime(temp, "%Y-%m-%d %H:%M:%S")
        self.updateCapLables()
        #self.updateDayView()
        self.updateAllTags(2, 0)     

    def flashDefect(self):   
        if self.flashCounter == 8 and self.autoClicked == True:
            self.DeActivateAllButtons()
            self.autoClicked = False

        if self.flashCounter < 8: 
            if self.flashCounter % 2 == 0:
                self.DefectLight.setPixmap(QtGui.QPixmap("RscImages/red"))
                self.ImageLabel.setStyleSheet("""border: 3px solid ;
                background-color: rgb(255, 255, 255);
                border-color: rgb(255, 0, 0)""")
                self.flashCounter = self.flashCounter + 1
            else:
                self.DefectLight.setPixmap(QtGui.QPixmap("RscImages/grey"))
                self.ImageLabel.setStyleSheet("""border: 3px solid ;
                background-color: rgb(255, 255, 255);
                border-color: rgb(0, 0, 0)""")
                self.flashCounter = self.flashCounter + 1
        else:
            self.flashCounter = 0
            self.timer.stop()
            self.DefectLight.setPixmap(QtGui.QPixmap("RscImages/grey"))
            self.ImageLabel.setStyleSheet("""border: 3px solid ;
            background-color: rgb(255, 255, 255);
            border-color: rgb(0, 0, 0)""")
    
    def DisplayCurrentTime(self):
        self.currentDateTime = self.roundHourDown(self.lastDefectDateTime)
        self.updateAllTags(2,0)
        self.updateCapLables()

    def roundHourDown(self, t):
        return t.replace(second=0, microsecond=0, minute=0)

    def exitHandler(self):
        self.LiveFeedThread.stop()
        self.defectThread.stop()
     
          
class LiveFeedThread(QtCore.QThread):
    ImageUpdate = pyqtSignal(QImage,QImage)

    def __init__(self, parent=None,cam=cv2.VideoCapture(0)):
        super(LiveFeedThread, self).__init__(parent)
        self.cam = cam

    def run(self):
        self.ThreadActive = True
        
        #camSet = 'nvarguscamerasrc sensor-id=1 ! video/x-raw(memory:NVMM), width=4056, height=3040, framerate=10/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width=640, height=854, format = BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink drop = True'

        #cam = cv2.VideoCapture(camSet)
        #cam = cv2.VideoCapture(0)

        while self.ThreadActive:
            ret, frame = self.cam.read()
            if ret:
                #live Feed
                #LiveFeedFrame = frame[100:580, 0:1080]
                LiveFeedFrame = frame


                Image = cv2.cvtColor(LiveFeedFrame, cv2.COLOR_BGR2RGB)
                FlippedImage = cv2.flip(Image, 1)
                #FlippedImage = Image
                ConvertToQtFormat = QImage(FlippedImage.data, FlippedImage.shape[1], FlippedImage.shape[0], QImage.Format_RGB888)
                PicLiveFeed = ConvertToQtFormat.scaled(854, 640, Qt.KeepAspectRatio)
                #PicLiveFeed = ConvertToQtFormat.scaled(1080, 480)

                
                #rio
                roi = frame[270:370, 0:854]
                Image2 = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
                FlippedImage2 = cv2.flip(Image2, 1)
                #FlippedImage = Image
                ConvertToQtFormat2 = QImage(FlippedImage2.data, FlippedImage2.shape[1], FlippedImage2.shape[0], QImage.Format_RGB888)
                #PicRoi = ConvertToQtFormat2.scaled(854, 100, Qt.KeepAspectRatio)
                PicRoi = ConvertToQtFormat2.scaled(854, 100)



                self.ImageUpdate.emit(PicLiveFeed,PicRoi)

                if self.ThreadActive == False:
                    break

    def stop(self):
        self.ThreadActive = False
        print('Stopping thread...image')
        self.terminate()
        

class ThreadClass(QtCore.QThread):
	
    update_signal = QtCore.pyqtSignal(int,object)
    def __init__(self, parent=None,lastDefectDateTime=0):
        super(ThreadClass, self).__init__(parent)
        self.lastDefectDateTime = lastDefectDateTime
        self.is_running = True

    def run(self):
        self.databaseHandler = dbH.DatabaseHandler("sqlite3/ImageSet.db")

        while True:

            result = self.databaseHandler.selectWhereDateWhereGreater("images",str(self.lastDefectDateTime))
            self.update_signal.emit(len(result),result)
            if len(result) > 0:
                tempDate = datetime.datetime.strptime(result[0][0], "%Y-%m-%d %H:%M:%S")
                self.changeLastTime(tempDate)
            #time.sleep(0.01) 

            time.sleep(0.01) 
            if self.is_running == False:
                break
        self.databaseHandler.close() # must kill it here as stop will be called in another thread
        print('connection closed...defect')


    def stop(self):
        self.is_running = False
        print('Stopping thread...defect')
        time.sleep(0.03) 
        self.terminate()

    def changeLastTime(self, lastDefectDateTime):
        self.lastDefectDateTime = lastDefectDateTime
        


# def GuiThreadStarter(starter, cam):
#     app = QtWidgets.QApplication(sys.argv)
#     mainWindow = Defect_Gui_App()
#     mainWindow.setupUi(mainWindow, cam)
#     mainWindow.show()
#     app.aboutToQuit.connect(mainWindow.exitHandler)
#     app.exec_()
#     mainWindow.timer.stop()
#     mainWindow.databaseHandler.close()

#     #sys.exit(app.exec_())

def GuiThreadStarter(cam):
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = Defect_Gui_App()
    mainWindow.setupUi(mainWindow, cam)
    mainWindow.show()
    app.aboutToQuit.connect(mainWindow.exitHandler)
    app.exec_()
    mainWindow.timer.stop()
    mainWindow.databaseHandler.close()

# app = QtWidgets.QApplication(sys.argv)
# mainWindow = Defect_Gui_App()
# mainWindow.setupUi(mainWindow)
# mainWindow.show()
# sys.exit(app.exec_())