import cv2
import time
from pathlib import Path
import threading
import queue
import DatabaseHandler as dbH
from datetime import datetime
#from mainGui import stop_threads

#closing = False ## temp soloution


class defectSaver(threading.Thread):
  # overriding constructor
    def __init__(self,imageQ, i):
    # calling parent class constructor
        threading.Thread.__init__(self)
        self.stop = False
        #self.setDaemon = True
        self.imageQ = imageQ
        self.i = i
    
  # define your own run method
    def run(self):
        databaseHandler = dbH.DatabaseHandler("sqlite3/ImageSet.db")
        while True:
            #print(self.stop, self.i)

            if self.imageQ.full()== True:
                print("-------------------------full")

            if self.imageQ.empty() == False:
                arr = self.imageQ.get()  ## its waiting here
                count = arr[0] 
                directory = arr[1]
                roi = arr[2]

                if count < 9:
                    idstring = "im0000" + str(count)
                elif count < 99:
                    idstring = "im000" + str(count)
                elif count < 999:
                    idstring = "im00" + str(count)
                elif count < 9999:
                    idstring = "im0" + str(count)

                
                now = datetime.now()
                currnetTime = str(now)

                if cv2.imwrite(directory  + '/' + str(count) + '.jpg', roi):
                    path = directory  + '/' + str(count) + '.jpg'
                    print('Thread: '+ str(self.i)+ " " + directory  + '/' + str(count) + '.jpg')
                    databaseHandler.insertEntry("images",[idstring,path,currnetTime[0:19],'bt001'])

                    #count += 1
                else:
                    print(roi.shape)
                    print(directory  + '/' + str(count) + '.jpg')
                    print('no write')
                    #count += 1   

            if self.stop == True:
                break

        databaseHandler.close()
        print("connection closed...defect Inputting", self.i)
        
    def terminateThread(self):
        self.stop = True





class imageCaptureTread(threading.Thread):
  # overriding constructor
    def __init__(self, cam):
    # calling parent class constructor
        threading.Thread.__init__(self)
        self.stop = False
        self.setDaemon = True
        self.cam = cam

    
  # define your own run method
    def run(self):
        time.sleep(3)
        print (cv2.__version__)
        timeMark = time.time()
        dtFil = 0
        font = cv2.FONT_HERSHEY_DUPLEX

        # Image directory
        directory = r'FabricImages'

        #camSet = 'nvarguscamerasrc sensor-id=1 ! video/x-raw(memory:NVMM), width=4056, height=3040, framerate=30/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width=3840, height=2880, format = BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink drop = True'
        #" ! appsink nvoverlaysink'q"""

        #cam = cv2.VideoCapture(camSet)
        #cam = cv2.VideoCapture(0)
        #the  ROI RECTANGLE coords
        xstartpoint = 120
        ystartpoint = 1600
        xendpoint = 3800
        yendpoint = 1800
        count = 10


        
        imageQ = queue.Queue()
        
        # workerArr = []
        # for i in range(2):
        #     #worker = threading.Thread(target=saveRoiThread, args=(imageQ, i), daemon=True)
        #     worker = defectSaver(imageQ,i)
        #     workerArr.append(worker)
        #     worker.start()
        worker1 = defectSaver(imageQ,1)
        worker1.start()
        worker2 = defectSaver(imageQ,2)
        worker2.start()

        while True:
            #get the frame (image)
            try:
                ret, frame = self.cam.read()
                print('frame: ',frame.shape)
            except:
                self.cam.release()
                cv2.destroyAllWindows()   # kills code if cam breaks
                exit()

                            
            #get gframes per sec.
            dt=time.time()-timeMark
            timeMark=time.time()
            dtFil=0.9*dtFil+0.1*dt
            fps=1/dtFil
            print('fps: ',fps)
            
            #the ROI coords
            ytop = ystartpoint
            ybot = yendpoint
            xleft = xstartpoint
            xright = xendpoint

            #get ROI (region of interest)

            #roi = frame[ytop:ybot, xleft:xright] # ys then xs      ##### uncomment on nano      
            roi = frame            ## uncomment on laptop
            
            #cv2.imshow('myCam',frame) 
            #cv2.moveWindow('myCam',0,0)

            if count % 100 == 0:
                imageQ.put([count,directory,roi])
                print('size:--------------------' , str(imageQ.qsize()))

            count += 1  
            #save the image
            if self.stop == True:
                break
        
        print("image capture stopping")
        worker1.terminateThread()
        worker1.join()
        #time.sleep(3)
        worker2.terminateThread()
        worker2.join()
        # for i in workerArr:
        #     i.terminateThread()
        #     time.sleep(0.5)
            

    
    def terminateThread(self):
        self.stop = True




